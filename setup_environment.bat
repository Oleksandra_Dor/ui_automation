pip install virtualenv
python -m venv ./venv
.\venv\Scripts\activate
pip install -r requirements.txt

webdrivermanager firefox chrome --linkpath .\drivers --downloadpath .\drivers