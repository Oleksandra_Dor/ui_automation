*** Settings ***
Resource  ../Resources/StudyDriveApp.robot
Resource  ../Resources/CommonWeb.robot
Documentation  This is Login and Logout to Studydrive Robot Test
Library  SeleniumLibrary
Library  DebugLibrary
Test Setup  Begin Web Test
Test Teardown  End Web Test


# robot -d results tests/studydrive.robot
*** Variables ***
${BROWSER}   googlechrome
${HOMEPAGE}  https://www.staging-01.studydrive.net/
${EMAIL}   oleksandra@studydrive.net
${PASSWORD}   123456
${USERNAME}   Oleksandra Doroshenko

*** Test Cases ***
Active User Can Login
    [Documentation]  Login and Logout to Studydrive as Normal User
    [Tags]  Smoke   Login
    StudyDriveApp.Accept Usercentrics Cookies
    StudyDriveApp.Login as Valid User
    StudyDriveApp.Logout



