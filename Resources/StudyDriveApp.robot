*** Settings ***
Resource  ./PO/Landing.robot
Resource  ./PO/Newsfeed.robot
Resource  ./PO/TopNav.robot
Resource  ./PO/Usercentrics.robot
*** Variables ***

*** Keywords ***
Accept Usercentrics Cookies
    Usercentrics.Hide Usercentrics

Login as Valid User
    TopNav.Click on "Login" Button
Logout
    Newsfeed.Click on "Logout" Menu Item