*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${ACCEPT_ALL}  dom:document.querySelector("#usercentrics-root").shadowRoot.querySelector("[data-testid='uc-accept-all-button']")

*** Keywords ***
Hide Usercentrics
    Click Button  ${ACCEPT_ALL}