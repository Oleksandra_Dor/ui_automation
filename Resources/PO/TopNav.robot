*** Settings ***
Library  SeleniumLibrary
*** Variables ***
${EMAIL_FIELD}   //input[@class="mb-4 border"]
${PASSWORD_FIELD}   //input[@class="mb-5 border"]
${BUTTON_LOGIN}   xpath=(//span[@class="leading-6"])[3]
${TEXT_BUTTON_LOGIN}   Log in
*** Keywords ***
Type Credentials
    Page Should Contain Textfield  ${EMAIL_FIELD}
    Page Should Contain Textfield  ${PASSWORD_FIELD}
    Input Text  ${EMAIL_FIELD}    ${EMAIL}
    Input Text  ${PASSWORD_FIELD}    ${PASSWORD}

Click on "Login" Button
    Page Should Contain Button  ${TEXT_BUTTON_LOGIN}
    Click Button  ${TEXT_BUTTON_LOGIN}
    Type Credentials
    Page Should Contain Element  ${BUTTON_LOGIN}
    Click Element  ${BUTTON_LOGIN}
    Wait Until Page Contains  Hey ${USERNAME}