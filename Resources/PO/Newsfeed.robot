*** Settings ***
Library  SeleniumLibrary
*** Variables ***
${PROFILE_IMAGE}  //img[@class="rounded-full w-6 h-6 md:w-10 md:h-10"]
${ICON_SETTINGS}   //i[@class="w-4 h-4 md:w-5 md:h-5 icon icon-settings"]
${OPTION_LOGOUT}   xpath=(//span[@class="ml-3"])[3]
${LOCATION_LOGOUT}   ${HOMEPAGE}en/companies-search
*** Keywords ***
Click on "Logout" Menu Item
    Click Element  ${PROFILE_IMAGE}
    Click Element  ${ICON_SETTINGS}
    Click Element  ${OPTION_LOGOUT}
    Location Should be  ${LOCATION_LOGOUT}