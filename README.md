# UI test automation scripts. #

### What is this repository for? ###

* Web UI automated test cases for Studydrive platform.
* Version 0.0.1

### How do I get set up? ###

#### Prerequirements ####
Python 3.9 installed in system

#### Summary of set up ####
 For Windows: launch **setup_environment.bat** file
  
#### Configuration: ####
TBD

#### How to run tests: ####
1. Activate virtual environment:
```
D:\ui_automation> venv\Scripts\activate
```

2. Launch example test case:
```
(venv) D:\ui_automation>robot tests\test_open.robot
```

### Contribution guidelines ###

* Writing tests: TBD
* Code review: TBD
* Other guidelines: TBD

### Who do I talk to? ###

* With any questions feel free to contact: oleksandra@studydrive.net